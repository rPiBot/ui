$(document).ready(function(){
    var pressed = 0;
    get_log();

    if($('#detected_output').length > 0){
        update_detected_image();
    }

    $(document).keydown(function(e) {
      if(pressed != e.which){
        pressed = e.which;

        var x = parseInt($('#cam_x').text());
        var y = parseInt($('#cam_y').text());

        switch(e.which) {
            case 37:  // LEFT
                look(x + 20, y);
                break;
            case 39:  // RIGHT
                look(x - 20, y);
                break;
            case 38:  // UP
                look(x, y - 20);
                break;
            case 40:  // DOWN
                look(x, y + 20);
                break;
            case 87:  // W
                move('forwards');
                break;
            case 65:  // A
                move('left');
                break;
            case 83:  // S
                move('backwards');
                break;
            case 68:  // D
                move('right');
                break;
            case 81:   // Q
                move('forwards_left');
                break;
            case 69:   // E
                move('forwards_right');
                break;
            case 90:   // Z
                move('backwards_left');
                break;
            case 88:  // X
                move('backwards_right');
                break;
            case 82:  // R
                look(90, 90);
                break;
            default:
                console.log(e.which);
        }
      }
    });

    $(document).keyup(function(e) {
      if(e.which == 87 || e.which == 65 || e.which == 83 || e.which == 68 || e.which == 81 || e.white == 69 || e.which == 90 || e.which == 88){
        move('stopped');
      }
      pressed = 0;
    });

    $('body').on('mousedown', '#move-controls a', function(e){
      var direction = $(this).prop('id').split('-')[1];
      move(direction);
      e.preventDefault();
    });

    $('body').on('mouseup, mouseleave', '#move-controls a', function(e){
      move('stopped');
      e.preventDefault();
    });

    $('body').on('mousedown', '#look', function(evt){
      var clicked = document.getElementById('look').getBoundingClientRect();

      var x = (180 - ((evt.clientX - clicked.left)) / 2).toFixed(0);
      var y = ((evt.clientY - clicked.top) / 2).toFixed(0);

      look(x, y);

    });
});

function update_detected_image(){
  $.ajax({
    url: "detected_image.php",
    method: "GET",
    dataType: 'text',
    mimeType: 'text/plain; charset=x-user-defined'
  })
  .done(function(data) {
    $('#detected_output').html(data);
    setTimeout(function(){ update_detected_image() }, 500);
  });
}

function look(x, y){
  if(x > 180) x = 180;
  if(y > 180) y = 180;
  if(x < 0) x = 0;
  if(y < 0) y = 0;

  $.ajax({
    url: "actions/look.php",
    method: "POST",
    data: { x: x, y: y },
    dataType: 'text',
    mimeType: 'text/plain; charset=x-user-defined'
  })
  .done(function(data){
    $('#cam_x').text(x);
    $('#cam_y').text(y);

    var c_x = (180 - (x * 2)) + 180;
    var c_y = y * 2;

    $('#marker').stop().animate({'top':c_y+'px', 'left':c_x+'px'});
  });
}

function get_log(){
  $.ajax({
    url: "log.php",
    method: "GET",
    dataType: 'text',
    mimeType: 'text/plain; charset=x-user-defined'
  })
  .done(function(data) {
    $('#log').html(data);
    $("#log").scrollTop($("#log")[0].scrollHeight);

    setTimeout(function(){ get_log() }, 500);
  });
}

function move(direction){
  $.ajax({
    url: "actions/move.php",
    method: "POST",
    data: { direction: direction },
    dataType: 'text',
    mimeType: 'text/plain; charset=x-user-defined'
  })
  .done(function( data ) {
    $('#move-controls a.btn-success').addClass('btn-info');
    $('#move-controls a.btn-success').removeClass('btn-success');

    $('#move-'+direction).removeClass('btn-info');
    $('#move-'+direction).addClass('btn-success');

    $('#move-status').html(direction);
  });
}
