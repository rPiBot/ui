<?php
$file_path = '/home/pi/core/rpibot.log';

$line_count = $_GET['lines'];
if(!$line_count){
  $line_count = 25;
}

if($line_count == 'all'){
  $file = fopen($file_path, "r") or die("Unable to open file!");
  $lines = fread($file,filesize($file_path));?>

  <!DOCTYPE html>
  <head>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <title>rPiBot</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
  </head>
  <body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 text-right">This page will not auto-refresh. <a href="log.php?lines=all&time=<?php echo time(); ?>" class="btn btn-info">Refresh now</a></div>
    </div>
    <div id="log_full" class="log">
<?php
} else {
  $lines = `tail -$line_count $file_path`;

  echo '<a class="pull-right" href="log.php?lines=all" target="_blank">View all</a>Last '.$line_count.' lines:<br/>';
}

echo nl2br($lines);

if($line_count == 'all'){ ?>
    </div>
  </div>
</body>
<?php }

?>
