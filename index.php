<?php include "services.php" ?>


<!DOCTYPE>
<html>
  <head>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/controls.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <title>rPiBot</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
  </head>

  <body>
    <div id="services">
      <?php if($pibot){ ?>
        <a href="?rpibot=stop" class="btn btn-danger">STOP SERVICE</a>
      <?php } else { ?>
        <a href="?rpibot=start" class="btn btn-success">START SERVICE</a>
      <?php } ?>

    </div>

    <div id="footer">
      <div class="row">
        <div class="col-md-6 col-md-offset-6 text-right">
          <?php foreach(array('manual', 'random', 'detection') as $control_type){ ?>
              <a class="btn btn-<?php if($control == $control_type){ ?>success<?php } else { ?>info<?php } ?> btn-xs" href="?control=<?php echo $control_type; ?>"><?php echo strtoupper($control_type); ?> CONTROL</a>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <h1>r<strong>Pi</strong>Bot <small></small></h1>
      <div id="main" class="row">
        <div id="move-controls" class="col-md-4 text-center full-height">
          <?php if($control == 'manual'){?>
            <h2>Move</h2>
            <p>&nbsp;</p>

  	         <a id="move-forwards_left" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-arrow-up"></span>&#8203;</a>
             <a id="move-forwards" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-arrow-up"></span>&#8203;</a>
             <a id="move-forwards_right" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-arrow-up"></span>&#8203;</a>
  	          <br/>
            <div class="btn-group">
              <a id="move-left" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-arrow-left"></span>&#8203;</a>
              <a class="spacer btn disabled"></a>
              <a id="move-right" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-arrow-right"></span>&#8203;</a>
            </div>
            <br/>
  	         <a id="move-backwards_left" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-arrow-down"></span>&#8203;</a>
             <a id="move-backwards" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-arrow-down"></span>&#8203;</a>
  	         <a id="move-backwards_right" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-arrow-down"></span>&#8203;</a>
            <br/>
            <br/>
            <div id="move-status" class="text-uppercase">
              STOPPED
            </div>
          <?php } elseif($control == 'detection') { ?>
            <h2>Camera Vision</h2>
            <div id="detected_output"></div>
          <?php } ?>
        </div>
        <div class="col-md-8 full-height">
          <h2 class="text-center">Log</h2>
          <div id="log" class="log"></div>
        </div>

      </div>
    </div>
  </body>
</html>
